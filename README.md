# Mooseland Adventure

A collaborative text adventure

![Screenshot of game running](screenshots/2019-03-26.png)

## How to run

Make sure you have Python3 installed on your computer.

From the **src** folder, run the **main.py** file with Python 3.

## How to make changes

Make a [**fork** of this repository](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html)
modify your changes, and then [make a **pull request**](https://www.atlassian.com/git/tutorials/making-a-pull-request).