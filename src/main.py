#!/usr/bin/python
# -*- coding: utf-8 -*-

print( "\n\n" )

from LocationManager import LocationManager
locationManager = LocationManager()

done = False

directionCommands = [ "north", "south", "east", "west" ]

def ShowHelp():
    print( "\n\n Commands: north, south, east, west \t help \t quit \n\n" )

while ( done == False ):
    locationManager.Display()
    
    command = input( "Enter a command: " ).lower()
    
    if ( command in directionCommands ):
        locationManager.Move( command )
    
    elif ( command == "help" ):
        ShowHelp()
        
    elif ( command == "quit" ):
        done = True
        

