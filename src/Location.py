#!/usr/bin/python
# -*- coding: utf-8 -*-

class Location:
    """
    Initialize the location item

    Parameters
    ----------

    title :         string      Name of the location
    description :   string      Text description of the location
    neighbors :     dictionary  Stores Location key of a neighbor to any given direction
    """
    def __init__( self, title="", description="", neighbors={ "north" : None, "south" : None, "east" : None, "west" : None } ):
        # Member variables
        self.title = title
        self.description = description
        self.neighbors = neighbors

    """
    Can go in a direction of that direction isn't set to None

    Parameters
    ----------
    direction : string      "north", "south", "east", or "west"

    Returns
    -------
    bool    Whether the player can move in that direction
            (Whether this location has a neighbor in that direction)
    """
    def CanGo( self, direction ):
        return ( self.neighbors[ direction ] is not None )
    
    """
    Get the neighbor location (key)

    Parameters
    ----------
    direction : string      "north", "south", "east", or "west"
    
    Returns
    -------
    string      Another location's key
    """
    def GetNeighbor( self, direction ):
        if ( self.neighbors[ direction ] is None ):
            raise ValueError( "Unable to move in direction \"" + direction + "\"; it is None!" )
        
        return self.neighbors[ direction ]

    """
    Print the Location information in an easy-to-read way
    """
    def Display( self ):
        print( self.title )
        print( self.description )
        
        print( "" )
        print( "You can move: " )
        
        directions = [ "north", "south", "east", "west" ]
        
        first = True
        for d in directions:
            if ( self.CanGo( d ) ):
                if ( first == False ):
                    print( ", ", end = " " )
                print( d, end = " " )
                first = False

        print( "\n" )








