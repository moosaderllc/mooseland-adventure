#!/usr/bin/python
# -*- coding: utf-8 -*-

from Location import Location

class LocationManager:
    def __init__( self ):
        self.locations = {}
        self.currentLocation = None
        self.startingLocation = None
        self.endingLocation = None
        self.Setup()
        self.moved = True

    def AddLocation( self, key, location ):
        if key in self.locations:
            raise ValueError( "Key \"" + key + "\" is already taken" )

        self.locations[key] = location

    def GetLocation( self, key ):
        if key not in self.locations:
            raise ValueError( "Key \"" + key + "\" not found" )

        return self.locations[key]
        
    def Display( self ):
        if ( self.moved ):
            self.currentLocation.Display()

        self.moved = False
        
    def Move( self, direction ):
        if ( self.currentLocation.CanGo( direction ) ):
            nextLocationKey = self.currentLocation.GetNeighbor( direction )
            self.currentLocation = self.locations[ nextLocationKey ]
            
            print( "Moved " + direction + "." )
            print( "\n\n" )
            self.moved = True
        
        else:
            print( "You can't go in that direction!" )
            print( "\n\n" )

    # TODO: This should ideally be stored in a separate file to be loaded in,
    # but for now we can manually set up the locations here.
    def Setup( self ):
        self.locations = {
            "castle-bedroom"    : Location( "Castle bedroom",   "You are in your bedroom in the castle.", { 
                "north" : None, 
                "south" : "castle-hallwayw", 
                "east" : None, 
                "west" : None 
                } ),
            
            "castle-hallwayw"   : Location( "Castle hallway west corner",    "At this end of the hallway there's a window looking out to the gardens.", { 
                "north" : "castle-bedroom", 
                "south" : None, 
                "east" : "castle-hallwaymid", 
                "west" : None 
                } ),
            
            "castle-hallwaymid" : Location( "Castle hallway center",    "The castle hallway is decorated with a fine red rug and tapestries on the wall.", { 
                "north" : None, 
                "south" : "castle-entrance", 
                "east" : None, 
                "west" : None 
                } ),
            
            "castle-hallwaye" : Location( "Castle hallway east corner",    "The window on this side of the hallway looks out on the lake.", { 
                "north" : None, 
                "south" : None, 
                "east" : None, 
                "west" : "castle-hallwaymid" 
                } ),
            
            "castle-throneroom" : Location( "Castle throne room",    "There is a grand throne in the center of this room, carpet running up to its feet.", { 
                "north" : None, 
                "south" : "castle-hallwaye", 
                "east" : None, 
                "west" : None 
                } ),
            
            "castle-entrance" : Location( "Castle entrance",    "The grand castle door stands open, allowing castle staff to freely move in and out.", { 
                "north" : "castle-hallwaymid", 
                "south" : "castle-grounds", 
                "east" : None, 
                "west" : None 
                } ),
            
            "castle-grounds" : Location( "Castle grounds",    "The castle stands tall before you.", { 
                "north" : "castle-entrance", 
                "south" : None, 
                "east" : None, 
                "west" : None 
                } )
        }
        
        self.startingLocation = self.locations["castle-bedroom"]
        self.endingLocation = self.locations["castle-grounds"]
        self.currentLocation = self.startingLocation
            
            
            
    
        
